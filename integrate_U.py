import numpy as np

def integrate(x, f):
    result = 0
    for i in range(len(x) - 1):
        result += (f[i] + f[i+1]) / 2.0 * (x[i+1] - x[i])
    return result
