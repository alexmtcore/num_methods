import os

def readtabs(filename):
    arg_x = []
    agr_y = []

    if os.path.exists(filename):
        file_in = open(filename, "r")

        for line in file_in:
            arg_x.append(line.split()[0])
            arg_y.append(line.split()[1])

        file_in.close()
    elif IOError:
        print("Unable to open file: " + str(filename))

    return arg_x, arg_y

def writetabs(arg_x, arg_y, filename):
    file_out = open(filename, 'w+')

    for i in range(len(arg_x)):
        file_out.write(str(arg_x[i]) + " " + str(arg_y[i]) + "\n")

    file_out.close()

def writecoeff(coeff, filename):
    file_out = open(filename, 'w+')

    for c in coeff:
        file_out.write(str(coeff) + "\n")

    file_out.close()
