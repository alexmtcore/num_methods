from lin_int import *
from read_write import *
import numpy as np

def interpolate():
    writecoeff([0 for i in range(100)], "pro_coeff.txt")
    writecoeff([0 for i in range(100)], "z_coeff.txt")
    writecoeff([0 for i in range(100)], "s_coeff.txt")
    writecoeff([0 for i in range(100)], "U_coeff.txt")
    LinSys()
    pass

def swap_max_row(A_modified, step):
    idx = step + np.argmax(np.abs(A_modified[step:, step]))
    if idx != step: A_modified[step, :], A_modified[idx, :] = np.copy(A_modified[idx, :]), np.copy(A_modified[step, :])

def solve_gauss(A, b):
    n = A.shape[0]
    A_modified = np.concatenate((A,b), axis=1)

    # forward
    for step in range(n - 1):
        swap_max_row(A_modified, step)
        for i in range(step + 1, n):
            fraction = A_modified[i, step] / A_modified[step, step]
            A_modified[i, :] -= A_modified[step, :] * fraction

    # backward
    x = np.zeros((n, 1), dtype=float)
    x[n-1] = A_modified[n-1, -1] / A_modified[n-1, n-1]
    for i in range(n - 2, -1, -1):
        x[i] = (A_modified[i, -1] - A_modified[i, (i+1):-1] * x[i+1:]) / A_modified[i, i]
    return x

class spline:
    def __init__(self):
         pass

    def fit(self, x, y):
        self.x = x
        self.get_ls(x, y)
        self.c = [0]
        c_k = solve_gauss(self.A, self.r)
        for i in c_k:
            self.c.append(i)
        self.c.append(0)

        self.a = y
        self.b, self.d = [], []
        for i in range(len(x) - 1):
            self.d.append(1.0 / (3 * self.delta[i]) * (self.c[i+1] - self.c[i]))
            self.b.append(1.0 / self.delta[i] * (y[i+1]-y[i]) -
                          self.delta[i] / 3.0 * (self.c[i+1]+2*self.c[i]))

    def get_ls(self, x, y):
        self.delta = [x[i+1] - x[i] for i in range(len(x) - 1)]
        A = np.matrix([0 for i in range((len(x) - 2)**2)],
                      dtype=float).reshape((len(x) - 2, len(x) - 2))


        for i in range(A.shape[0]):
            if (i != 0) : A[i, i - 1] = self.delta[i]
            if (i != A.shape[0] - 1) : A[i, i + 1] = self.delta[i+1]
            A[i, i] = 2 * (self.delta[i] + self.delta[i+1])

        r = np.matrix([ 1.0 * (y[i+2] - y[i+1]) / self.delta[i+1]
                      - 1.0 * (y[i+1] - y[i]) / self.delta[i]
             for i in range(len(x) - 2)]).reshape((-1,1))
        self.A , self.r = A, r

    def get_value(self, x):
        if x < self.x[0]: k = 0
        else: k = len(self.x) - 2

        for i in range(len(self.x) - 1):
            if (x >= self.x[i] and x < self.x[i+1]):
                k = i
                break

        return (self.a[k] + self.b[k] *  (x - self.x[k]) +
                self.c[k] * (x - self.x[k]) ** 2 + self.d[k] * (x - self.x[k]) ** 3)[0]

    def get_diff_value(self, x):
        if x < self.x[0]: k = 0
        else: k = len(self.x) - 2

        for i in range(len(self.x) - 1):
            if (x >= self.x[i] and x < self.x[i+1]):
                k = i
                break

        return (self.b[k] +
                2 * self.c[k] * (x - self.x[k]) + 3 * self.d[k] * (x - self.x[k]) ** 2)[0]
