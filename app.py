from tkinter import *
import tkinter.messagebox
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt

from U_tab import *
from interpolation import *
from Cauchy import *
import read_write
from math import *
from criterions import *
from beta import *
import numpy as np

auto = 0

def destroy():
    exit(0)

S = spline()
p = spline()
z = spline()

s_func = lambda t: S.get_value(t)
z_func = lambda t: z.get_value(t)
p_func = lambda w: p.get_value(w)

def start_app():
    if (((def_prob or def_prob_load) + (def_z or def_z_load)
         + (def_s or def_s_load) + def_f) != 4):
        tkinter.messagebox.showinfo("Error", "Not all functions passed")
        return 42

    root.destroy()

def start_app_auto():
    if (((def_prob or def_prob_load) + (def_z or def_z_load)
         + (def_s or def_s_load) + def_f) != 4):
        tkinter.messagebox.showinfo("Error", "Not all functions passed")
        return 42

    global auto
    auto = 1
    root.destroy()

t_rk = []
x_rk = []
y_rk = []

str_f = ''
def f(x, z, S, Beta):
    return eval(str_f)

def f1(t, x, y):
    w_int = [y + (1-y) * i / 50 for i in range(51)]
    f_int = [p.get_value(w) for w in w_int]
    int_value = integrate(w_int, f_int)
    return z.get_diff_value(t) * int_value

def f2(t, x, y):
    global Beta
    return f(x, z.get_value(t), S.get_value(t), Beta)

def solve():
    global x0, y0, Beta, T
    v1 = str(x0_entry.get())
    v2 = str(y0_entry.get())
    v3 = str(beta_entry.get())
    s1 = v1.replace('.','',1).replace('-','',1)
    s2 = v2.replace('.','',1).replace('-','',1)
    s3 = v3.replace('.','',1).replace('-','',1)

    if ((s1.isdigit() and s2.isdigit() and s3.isdigit()) != True):
        tkinter.messagebox.showinfo("Error", "Incorrect params")
        return 42

    x0 = float(v1)
    y0 = float(v2)
    Beta = float(v3)

    global x_rk, t_rk, y_rk
    x_rk, y_rk, t_rk = rk(f1, f2, [0, T], x0, y0, 100)
    root.destroy()

def c1():
    global T, x0
    x_func = spline()
    x_func.fit(t_rk, x_rk)
    y_func = spline()
    y_func.fit(t_rk, y_rk)
    int_func = []
    t_int = [T / 100 * i for i in range(0, 101, 1)]
    res_int = []
    for t in t_int:
        y = y_func.get_value(t)
        w_int = [y + (1-y) * i / 50 for i in range(51)]
        f_int = [w * p.get_value(w) for w in w_int]
        int_value = integrate(w_int, f_int)
        res_int.append(int_value * x_func.get_diff_value(t))
    result = 1 - 1.0 / (x_func.get_value(t) - x0) * integrate(t_int, res_int)
    return result

def c2():
    x_func = spline()
    x_func.fit(t_rk, x_rk)
    return np.abs(x_func.get_value(T) - S.get_value(T)) / S.get_value(T)

Beta_left = 0
Beta_right = 0
A = 1
B = 10

def solve_auto():
    global x0, y0, Beta_left, Beta_right, T
    v1 = str(x0_entry.get())
    v2 = str(y0_entry.get())
    v3 = str(Beta_left_entry.get())
    v4 = str(Beta_right_entry.get())
    s1 = v1.replace('.','',1).replace('-','',1)
    s2 = v2.replace('.','',1).replace('-','',1)
    s3 = v3.replace('.','',1).replace('-','',1)
    s4 = v4.replace('.','',1).replace('-','',1)

    if ((s1.isdigit() and s2.isdigit() and s3.isdigit() and s4.isdigit()) != True):
        tkinter.messagebox.showinfo("Error", "Incorrect params")
        return 42

    x0 = float(v1)
    y0 = float(v2)
    Beta_left = float(v3)
    Beta_right = float(v4)

    global x_rk, t_rk, y_rk, Beta
    beta_list = [Beta_left + (Beta_right - Beta_left) / 5 * i for i in range(6)]
    min_val = 1000
    beta_opt = None
    beta_plot = []
    val_plot = []
    for b in beta_list:
        Beta = b
        x_rk, y_rk, t_rk = rk(f1, f2, [0, T], x0, y0, 100)
        if (np.any(np.array(x_rk) < 0) or np.any(np.array(y_rk) < 0)
            or np.any(np.array(y_rk) > 1)):
            continue
        cur_val = A * c1() + B * c2()
        val_plot.append(cur_val)
        beta_plot.append(b)
        if cur_val < min_val:
            min_val = cur_val
            beta_opt = b

    Beta = beta_opt
    print(Beta)
    sol_label = Label(frame, text="Optimal Beta for Cauchy problem is {}".format(beta_opt),
                       fg = 'red')
    sol_label.grid(row=5)

    get_button = Button(frame, text="Get (X0, Y0)", width=10,
                         command=root.destroy, background = 'red',
                         height = 2)
    get_button.grid(row=7)

    plt.plot(beta_plot, val_plot, marker='o')
    plt.show()
    root.destroy()

x_0 = 0
y_0 = 0
Beta = 0
T = 0

def get_f():
    func = str(f_entry.get())
    global def_f
    try:
        x = 1
        z = 1
        S = 1
        Beta = 1
        eval(func)
        global str_f
        str_f = func
    except:
        def_f = 0
        tkinter.messagebox.showinfo("Error", "Incorrect f(x, z, S, Beta)")
        return 42

    def_f = 1

def prob():
    func = str(prob_entry.get())
    global def_prob
    try:
        w = 1
        eval(func)
    except:
        def_prob = 0
        tkinter.messagebox.showinfo("Error", "Incorrect prob density func pho(w)")
        return 42

    arg_w = [i * 1.0 / 100 for i in range(101)]
    arg_y = []
    for w in arg_w:
        arg_y.append(eval(func))

    global p
    p.fit(arg_w, arg_y)

    plt.plot(arg_w, [p.get_value(i) for i in arg_w])
    plt.show()

    def_prob = 1

def load_p():
    filename = str(load_prob_entry.get())
    global def_prob_load
    arg_w = []
    arg_y = []
    try:
        file_in = open(filename, "r")

        for line in file_in:
            arg_w.append(line.split()[0])
            arg_y.append(line.split()[1])

        file_in.close()
    except:
        def_prob_load = 0
        tkinter.messagebox.showinfo("Error", "Broken path")
        return 42

    global p
    p.fit(arg_w, arg_y)
    plt.plot(arg_w, [p.get_value(i) for i in arg_w])
    plt.show()

    def_prob_load = 1

def z_func():
    func = str(z_entry.get())
    global def_z
    try:
        t = 1
        eval(func)
    except:
        def_z = 0
        tkinter.messagebox.showinfo("Error", "Incorrect z(t) function")
        return 42

    arg_t = [T / 100 * i for i in range(0, 101, 1)]
    arg_y = []
    for t in arg_t:
        arg_y.append(eval(func))
    global z
    z.fit(arg_t, arg_y)
    plt.plot(arg_t, [z.get_value(i) for i in arg_t])
    plt.show()

    def_z = 1

def load_z():
    filename = str(load_z_entry.get())
    global def_z_load
    arg_t = []
    arg_y = []
    try:
        file_in = open(filename, "r")

        for line in file_in:
            arg_t.append(line.split()[0])
            arg_y.append(line.split()[1])

        file_in.close()
    except:
        def_z_load = 0
        tkinter.messagebox.showinfo("Error", "Broken path")
        return 42

    global z
    z.fit(arg_t, arg_y)
    plt.plot(arg_t, [z.get_value(i) for i in arg_t])
    plt.show()

    def_z_load = 1

def S_func():
    func = str(S_entry.get())
    global def_s
    try:
        t = 1
        eval(func)
    except:
        def_s = 0
        tkinter.messagebox.showinfo("Error", "Incorrect z(t) function")
        return 42

    arg_t = [T / 100 * i for i in range(0, 101, 1)]
    arg_y = []
    for t in arg_t:
        arg_y.append(eval(func))
    global S
    S.fit(arg_t, arg_y)
    plt.plot(arg_t, [S.get_value(i) for i in arg_t])
    plt.show()

    def_s = 1

def load_S():
    filename = str(load_S_entry.get())
    global def_s_load
    arg_t = []
    arg_y = []
    try:
        file_in = open(filename, "r")

        for line in file_in:
            arg_t.append(line.split()[0])
            arg_y.append(line.split()[1])

        file_in.close()
    except:
        def_s_load = 0
        tkinter.messagebox.showinfo("Error", "Broken path")
        return 42

    global S
    S.fit(arg_t, arg_y)
    plt.plot(arg_t, [S.get_value(i) for i in arg_t])
    plt.show()

    def_s_load = 1

def get_T():
    global def_s
    global T
    s = str(T_entry.get()).replace('.','',1)

    if (s.isdigit() != True):
        tkinter.messagebox.showinfo("Error", "Incorrect params")
        return 42

    T = float(s)
    root.destroy()

root = Tk()
root.title("Приложение")
frame = Frame(root)
frame.pack()

T_label = Label(frame, text="Enter T",
                   fg = 'red')
T_label.grid(row=0)

T_entry = Entry(frame, width=6)
T_entry.grid(row=1, column = 0)

next_button = Button(frame, text="Next", width=10,
                     command=get_T, background = 'red',
                     height = 2)
next_button.grid(row=2)

exit_button = Button(frame, text="Exit", width=10,
                     command=destroy, background = 'red',
                     height = 2)
exit_button.grid(row=3)
root.mainloop()

root = Tk()
root.title("Приложение")
frame = Frame(root)
frame.pack()
#prob(w)
prob_entry = Entry(frame, width=30)
prob_entry.grid(row=1)
prob_label = Label(frame, text="Enter prob density func pho(w)",
                   fg = 'red')
prob_label.grid(row=0)
prob_button = Button(frame, text="Tabulate and Show", width=28,
                     command=prob)
prob_button.grid(row=3)
load_prob_entry = Entry(frame, width=30)
load_prob_entry.grid(row=4)
load_prob_button = Button(frame, text="Load an Show", width=28,
                     command=load_p)
load_prob_button.grid(row=5)
#z(t)
z_entry = Entry(frame, width=30)
z_entry.grid(row=7)
z_label = Label(frame, text="Enter z(t) function", fg = 'red')
z_label.grid(row=6)
z_button = Button(frame, text="Tabulate and Show", width=28,
                     command=z_func)
z_button.grid(row=8, column=0)
load_z_entry = Entry(frame, width=30)
load_z_entry.grid(row=9)
load_z_button = Button(frame, text="Load an Show", width=28,
                     command=load_z)
load_z_button.grid(row=10)
#S(t)
S_entry = Entry(frame, width=30)
S_entry.grid(row=12)
S_label = Label(frame, text="Enter S(t) function", fg = 'red')
S_label.grid(row=11)
S_button = Button(frame, text="Tabulate and Show", width=28,
                     command=S_func)
S_button.grid(row=13)
load_S_entry = Entry(frame, width=30)
load_S_entry.grid(row=14)
load_S_button = Button(frame, text="Load an Show", width=28,
                     command=load_S)
load_S_button.grid(row=15)
#f(t)
f_entry = Entry(frame, width=30)
f_entry.grid(row=17)
f_label = Label(frame, text="Enter f(x, z, S, Beta) function", fg = 'red')
f_label.grid(row=16)
f_button = Button(frame, text="Remember f(x, z, S, Beta)", width=28,
                     command=get_f)
f_button.grid(row=18)
#flags
def_prob = 0
def_z = 0
def_s = 0
def_f = 0
def_prob_load = 0
def_z_load = 0
def_s_load = 0
#fuctional buttons
start_button = Button(frame, text="Start solution", width=10,
                     command=start_app, background = "yellow",
                     height = 2)
start_button.grid(row=20)

start_button_auto = Button(frame, text="Start in auto", width=10,
                     command=start_app_auto, background = "yellow",
                     height = 2)
start_button_auto.grid(row=21)

exit_button = Button(frame, text="Exit", width=10,
                     command=destroy, background = 'red',
                     height = 2)
exit_button.grid(row=22)
root.mainloop()
if not auto:
    #интерфейс ввода параметров для задачи Коши
    root = Tk()
    root.title("Приложение")
    frame = Frame(root)
    frame.pack()

    sol_label = Label(frame, text="Enter parameters x0, y0, Beta for Cauchy problem",
                       fg = 'red')
    sol_label.grid(row=0)

    x0_entry = Entry(frame, width=6)
    x0_entry.grid(row=1, column = 0)

    y0_entry = Entry(frame, width=6)
    y0_entry.grid(row=2, column = 0)

    beta_entry = Entry(frame, width=6)
    beta_entry.grid(row=3, column = 0)


    solve_button = Button(frame, text="Solve problem", width=28,
                         command=solve, height = 2)
    solve_button.grid(row=4)

    exit_button = Button(frame, text="Exit", width=10,
                         command=destroy, background = 'red',
                         height = 2)
    exit_button.grid(row=5)
    root.mainloop()

    #интерфейс отображения результатов
    root = Tk()
    root.title("Приложение")
    frame = Frame(root)
    frame.pack()

    #траектория (S, x)
    graph_label = Label(frame, text="Here graphic (S, x)",
                       fg = 'red')
    graph_label.grid(row=0)
    plt.plot(x_rk,[S.get_value(t) for t in t_rk])
    plt.plot(x_rk, x_rk, color='red')
    plt.show()
    graph_label.destroy()

    #график y(t)
    graph_label = Label(frame, text="Here graphic y(t)",
                       fg = 'red')
    graph_label.grid(row=0)
    plt.plot(t_rk, y_rk)
    plt.show()
    graph_label.destroy()

    #значения критериев
    c1_value = c1()
    c2_value = c2()
    c1_label = Label(frame, text="Value on C1 criteria is {}".format(c1_value),
                       fg = 'red')
    c1_label.grid(row=0)

    c2_label = Label(frame, text="Value on C2 criteria is {}".format(c2_value),
                       fg = 'red')
    c2_label.grid(row=1)



    #выход из приложения
    exit_button = Button(frame, text="Exit", width=10,
                         command=destroy, background = 'red',
                         height = 2)
    exit_button.grid(row=2)

    root.mainloop()
else:
    #интерфейс ввода параметров для задачи Коши
    root = Tk()
    root.title("Приложение")
    frame = Frame(root)
    frame.pack()

    sol_label = Label(frame, text="Enter parameters x0, y0, Beta_left, Beta_right for Cauchy problem",
                       fg = 'red')
    sol_label.grid(row=0)

    x0_entry = Entry(frame, width=6)
    x0_entry.grid(row=1, column = 0)

    y0_entry = Entry(frame, width=6)
    y0_entry.grid(row=2, column = 0)

    Beta_left_entry = Entry(frame, width=6)
    Beta_left_entry.grid(row=3, column = 0)

    Beta_right_entry = Entry(frame, width=6)
    Beta_right_entry.grid(row=4, column = 0)

    solve_button = Button(frame, text="Find Optimal Beta", width=28,
                         command=solve_auto, height = 2)
    solve_button.grid(row=6)

    exit_button = Button(frame, text="Exit", width=10,
                         command=destroy, background = 'red',
                         height = 2)
    exit_button.grid(row=8)
    root.mainloop()

    #области
    root = Tk()
    root.title("Приложение")
    frame = Frame(root)
    frame.pack()
    #prob(w)

    global x_rk, t_rk, y_rk, Beta, x0, y0, T
    if (Beta == None): destroy()
    print(Beta)
    x0_list = [x0 + i * 0.1 / 5 for i in range(6)]
    y0_list = [y0 + i * 0.1 / 5 for i in range(6)]
    x_rk_list, y_rk_list, t_rk_list = [], [], []
    for x0_cur in x0_list:
        for y0_cur in y0_list:
            x0, y0 = x0_cur, y0_cur
            x_rk, y_rk, t_rk = rk(f1, f2, [0, T], x0, y0, 100)
            if (np.any(np.array(x_rk) < 0) or np.any(np.array(y_rk) < 0)
                or np.any(np.array(y_rk) > 1)):
                continue
            x_rk_list.append(x_rk)
            y_rk_list.append(y_rk)
            t_rk_list.append(t_rk)

    x_label = Label(frame, text="Область значений X0",
                       fg = 'red')
    x_label.grid(row = 0, column = 0)
    x0_label = Label(frame, text="[{}; {}]".format(x0_list[0], x0_list[-1]),
                       fg = 'red')
    x0_label.grid(row = 0, column = 1)

    y_label = Label(frame, text="Область значений Y0",
                       fg = 'red')
    y_label.grid(row = 1, column = 0)
    y0_label = Label(frame, text="[{}; {}]".format(y0_list[0], y0_list[-1]),
                       fg = 'red')
    y0_label.grid(row = 1, column = 1)

    gr_label = Label(frame, text="Графики при разных значениях (X0, Y0) траектории (S, x)",
                       fg = 'red')
    gr_label.grid(row = 2, column = 0)



    exit_button = Button(frame, text="Exit", width=10,
                         command=destroy, background = 'red',
                         height = 2)
    exit_button.grid(row=3)


    for i in range(len(x_rk_list)):
        x_rk = x_rk_list[i]
        t_rk = t_rk_list[i]
        plt.plot(x_rk,[S.get_value(t) for t in t_rk])
    plt.show()

    gr_label.destroy()
    gr_label = Label(frame, text="Графики при разных значениях (X0, Y0) траектории y(t)",
                       fg = 'red')
    gr_label.grid(row = 2, column = 0)
    x_label.destroy()
    x0_label.destroy()
    y_label.destroy()
    y0_label.destroy()

    for i in range(len(y_rk_list)):
        y_rk = y_rk_list[i]
        t_rk = t_rk_list[i]
        plt.plot(t_rk,y_rk)
    plt.show()

    root.mainloop()
