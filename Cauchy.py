import numpy as np

def rk(f1, f2, t, x10, x20, num):
    T = [t[0] + 1.0 * (t[-1] - t[0]) * i / num for i in range(num + 1)]
    x1 = [x10]
    x2 = [x20]
    for i in range(1,num+1):
        # each step parameter
        h = T[i] - T[i-1]
        tn, x1n, x2n = T[i-1], x1[i-1], x2[i-1]

        k1 = f1(tn, x1n, x2n) * h
        m1 = f2(tn, x1n, x2n) * h

        k2 = f1(tn+h/2.0, x1n+1/2.0*k1, x2n+1/2.0*m1) * h
        m2 = f2(tn+h/2.0, x1n+1/2.0*k1, x2n+1/2.0*m1) * h

        m3 = f2(tn+h/2.0, x1n+1/2.0*k2, x2n+1/2.0*m2) * h
        k3 = f1(tn+h/2.0, x1n+1/2.0*k2, x2n+1/2.0*m2) * h

        m4 = f2(tn+h, x1n+k3, x2n+m3) * h
        k4 = f1(tn+h, x1n+k3, x2n+m3) * h

        x1.append(x1n+1/6.0*(k1+2*k2+2*k3+k4))
        x2.append(x2n+1/6.0*(m1+2*m2+2*m3+m4))
    return x1, x2, T
